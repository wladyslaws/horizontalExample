//
//  AppDelegate.h
//  Badges
//
//  Created by WLADYSLAW SURALA on 27/03/2017.
//  Copyright © 2017 wladyslaws. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

