//
//  ViewController.h
//  Badges
//
//  Created by WLADYSLAW SURALA on 27/03/2017.
//  Copyright © 2017 wladyslaws. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property IBOutlet UIImageView *badgeView1;
@property IBOutlet UIImageView *badgeView2;
@property IBOutlet UIImageView *badgeView3;
@property IBOutlet UIImageView *badgeView4;


@end

