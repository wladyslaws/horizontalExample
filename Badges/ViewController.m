//
//  ViewController.m
//  Badges
//
//  Created by WLADYSLAW SURALA on 27/03/2017.
//  Copyright © 2017 wladyslaws. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.badgeView2.hidden = YES;
    self.badgeView3.hidden = YES;

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
